import os
from dotenv import load_dotenv, find_dotenv
from datetime import datetime
from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse

load_dotenv(find_dotenv())

src_number = os.environ['SRC_NUMBER']
dst_number = os.environ['DST_NUMBER']

account_sid = os.environ['TWILIO_ACCOUNT_SID']
auth_token = os.environ['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)

response = VoiceResponse()
response.play(
    url='https://gitlab.com/ShironCat/troll-bot/-/raw/master/wmuwse.wav')
response.say(
    'Bom dia, Cíntia! Espero que tenha dormido bem. Setembro acabou, já está na hora de acordar!',
    language='pt-BR')
response.play(
    url='https://gitlab.com/ShironCat/troll-bot/-/raw/master/wmuwse.wav')

call = client.calls.create(
    twiml=response,
    to=dst_number,
    from_=src_number
)

print(datetime.now())
print(call.sid)
